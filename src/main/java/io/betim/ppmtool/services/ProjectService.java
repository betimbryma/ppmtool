package io.betim.ppmtool.services;

import io.betim.ppmtool.domain.Backlog;
import io.betim.ppmtool.domain.Project;
import io.betim.ppmtool.domain.User;
import io.betim.ppmtool.exceptions.ProjectIdException;
import io.betim.ppmtool.exceptions.ProjectNotFoundException;
import io.betim.ppmtool.repositories.BacklogRepository;
import io.betim.ppmtool.repositories.ProjectRepository;
import io.betim.ppmtool.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

    private ProjectRepository projectRepository;
    private BacklogRepository backlogRepository;

    public ProjectService(ProjectRepository projectRepository, BacklogRepository backlogRepository) {
        this.projectRepository = projectRepository;
        this.backlogRepository = backlogRepository;
    }

    @Autowired
    private UserRepository userRepository;

    public Project saveOrUpdateProject(Project project, String username){

        if(project.getId() != null){
            Project existingProject = projectRepository.findByProjectIdentifier(project.getProjectIdentifier());

            if(existingProject != null && (!existingProject.getProjectLeader().equals(username)))
                throw new ProjectNotFoundException("Project not found in your account");
            else if(existingProject == null)
                throw new ProjectNotFoundException("Project with ID "+ project.getId()+" does not exist");
        }

    try {

        User user = userRepository.findByUsername(username);
        project.setUser(user);
        project.setProjectLeader(user.getUsername());

        project.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());

        if(project.getId() == null){
            Backlog backlog = new Backlog();
            project.setBacklog(backlog);
            backlog.setProject(project);
            backlog.setProjectIdentifier(project.getProjectIdentifier());
        } else
            project.setBacklog(backlogRepository.findByProjectIdentifier(project.getProjectIdentifier()));


        return projectRepository.save(project);
    } catch (Exception e) {
        throw new ProjectIdException("Project ID '" + project.getProjectIdentifier().toUpperCase() +"' already exists");
    }

    }

    public Project findProjectByIdentifier(String projectId, String username){

        Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());

        if(project == null)
            throw new ProjectIdException("Project ID '" + projectId +"' does not exist");

        if(!project.getProjectLeader().equals(username))
            throw new ProjectNotFoundException("Project not found in your account");


        return projectRepository.findByProjectIdentifier(projectId.toUpperCase());
    }

    public Iterable<Project> findAllProjects(String username){
        return projectRepository.findAllByProjectLeader(username);
    }

    public void deleteProjectByIdentifier(String projectId, String username){

        projectRepository.delete(findProjectByIdentifier(projectId, username));
    }


}
