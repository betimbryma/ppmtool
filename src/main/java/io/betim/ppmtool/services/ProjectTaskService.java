package io.betim.ppmtool.services;

import io.betim.ppmtool.domain.Backlog;
import io.betim.ppmtool.domain.ProjectTask;
import io.betim.ppmtool.exceptions.ProjectNotFoundException;
import io.betim.ppmtool.repositories.BacklogRepository;
import io.betim.ppmtool.repositories.ProjectRepository;
import io.betim.ppmtool.repositories.ProjectTaskRepository;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class ProjectTaskService {

    private BacklogRepository backlogRepository;
    private ProjectTaskRepository projectTaskRepository;
    private ProjectRepository projectRepository;
    private ProjectService projectService;

    public ProjectTaskService(BacklogRepository backlogRepository, ProjectTaskRepository projectTaskRepository, ProjectRepository projectRepository, ProjectService projectService) {
        this.backlogRepository = backlogRepository;
        this.projectTaskRepository = projectTaskRepository;
        this.projectRepository = projectRepository;
        this.projectService = projectService;
    }

    public ProjectTask addProjectTask(String projectIdentifier, ProjectTask projectTask, String username) {


        Backlog backlog = projectService.findProjectByIdentifier(projectIdentifier, username).getBacklog();

        projectTask.setBacklog(backlog);

        Integer backlogSequence = backlog.getPTSequence();
        projectTask.setProjectSequence(projectIdentifier + (++backlogSequence));
        projectTask.setProjectIdentifier(projectIdentifier);
        backlog.setPTSequence(backlogSequence);


        if (projectTask.getPriority() == null || projectTask.getPriority() == 0)
            projectTask.setPriority(3);

        if (projectTask.getStatus() == null || projectTask.getStatus().trim().isEmpty())
            projectTask.setStatus("TO_DO");

        return projectTaskRepository.save(projectTask);


    }

    public Iterable<ProjectTask> findBacklogById(String backlog_id, String username) {

        projectService.findProjectByIdentifier(backlog_id, username);

        return projectTaskRepository.findByProjectIdentifierOrderByPriority(backlog_id);
    }

    public ProjectTask findProjectTaskByProjectSequence(String backlog_id, String pt_id, String username) {

        projectService.findProjectByIdentifier(backlog_id, username);

        ProjectTask projectTask = projectTaskRepository.findByProjectSequence(pt_id);
        if (projectTask == null)
            throw new ProjectNotFoundException("Project Task with ID: '" + pt_id + "' does not exist");

        if (!projectTask.getProjectIdentifier().equals(backlog_id))
            throw new ProjectNotFoundException("Project Task does not exist");

        return projectTask;
    }

    public ProjectTask updateByProjectSequence(ProjectTask updatedTask, String backlog_id, String pt_id, String username) {

        projectTaskRepository.findByProjectSequence(updatedTask.getProjectSequence());

        return projectTaskRepository.save(updatedTask);

    }

    public void deleteProjectTaskByProjectSequence(String backlog_id, String pt_id, String username) {
        ProjectTask projectTask = findProjectTaskByProjectSequence(backlog_id, pt_id, username);

        projectTaskRepository.delete(projectTask);
    }

}
