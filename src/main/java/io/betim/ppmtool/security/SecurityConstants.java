package io.betim.ppmtool.security;

public class SecurityConstants {

    static final String SIGN_UP_URLS = "/api/users/**";
    static final String H2_URL = "h2-console/**";
    static final String SECRET = "kowjgui2whUHQUOHRQ737Hajsnfjhabfgha#awrga";
    public static final String TOKEN_PREFIX = "Bearer ";
    static final String HEADER_STRING = "Authorization";
    static final long EXPIRATION_TIME = 300000L;

}
