package io.betim.ppmtool.exceptions;

public class ProjectNotFoundExceptionResponse {

    private String projectNotFoud;

    public ProjectNotFoundExceptionResponse(String projectNotFoud) {
        this.projectNotFoud = projectNotFoud;
    }

    public String getProjectNotFoud() {
        return projectNotFoud;
    }

    public void setProjectNotFoud(String projectNotFoud) {
        this.projectNotFoud = projectNotFoud;
    }
}
