import {createStore, applyMiddleware, compose} from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";

const initialState = {};
const middleware = [thunk];

const reactReduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

let store;

if(reactReduxDevTools)
    store = createStore(rootReducer, initialState, compose(applyMiddleware(...middleware), reactReduxDevTools));
else
    store = createStore(rootReducer, initialState, compose(applyMiddleware(...middleware)));

export default store;