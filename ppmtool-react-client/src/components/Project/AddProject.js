import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {createProject} from "../../actions/projectActions";
import classnames from "classnames";

class AddProject extends Component {

    constructor(){
        super();

        this.state = {
            projectname: "",
            projectIdentifier: "",
            description: "",
            start_date: "",
            end_date: "",
            errors: {}
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.errors){
            this.setState({errors: nextProps.errors})
        }
    }

    onChange(e){
        this.setState({[e.target.name]:e.target.value})
    }

    onSubmit(e) {
        e.preventDefault();
        const newProject = {
            projectname: this.state.projectname,
            projectIdentifier: this.state.projectIdentifier,
            description: this.state.description,
            start_date: this.state.start_date,
            end_date: this.state.end_date
        };

        this.props.createProject(newProject, this.props.history)
    }

    render() {
        let {errors} = this.state;

        return (
            <div className="project">

                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h5 className="display-4 text-center">Create / Edit Project form</h5>
                            <hr/>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.projectname
                                    })}
                                           placeholder="Project Name"
                                           name="projectname"
                                           onChange={this.onChange}
                                            value={this.state.projectname}/>
                                    {errors.projectname && (
                                        <div className="invalid-feedback">{errors.projectname}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {"is-invalid": errors.projectIdentifier})}
                                           placeholder="Unique Project ID"
                                           name="projectIdentifier"
                                           onChange={this.onChange}
                                    value={this.state.projectIdentifier}/>
                                    {errors.projectIdentifier && (
                                        <div className="invalid-feedback">{errors.projectIdentifier}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <textarea className={classnames("form-control form-control-lg", {"is-invalid": errors.description})}
                                              placeholder="Project Description"
                                                name="description"
                                              onChange={this.onChange}
                                                value={this.state.description}/>
                                    {errors.description && (
                                        <div className="invalid-feedback">{errors.description}</div>
                                    )}
                                </div>
                                <h6>Start Date</h6>
                                <div className="form-group">
                                    <input type="date" className={classnames("form-control form-control-lg", {"is-invalid": errors.start_date})}
                                           name="start_date"
                                           onChange={this.onChange}
                                            value={this.state.start_date}/>
                                    {errors.start_date && (
                                        <div className="invalid-feedback">{errors.start_date}</div>
                                    )}
                                </div>
                                <h6>Estimated End Date</h6>
                                <div className="form-group">
                                    <input type="date" className={classnames("form-control form-control-lg", {"is-invalid": errors.end_date})}
                                           name="end_date"
                                           onChange={this.onChange}
                                            value={this.state.end_date}/>
                                    {errors.end_date && (
                                        <div className="invalid-feedback">{errors.end_date}</div>
                                    )}
                                </div>
                                <p>{errors.end_date}</p>
                                <input type="submit" className="btn btn-primary btn-block mt-4"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

AddProject.propTypes = {
    createProject: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    errors: state.errors
});

export default connect(mapStateToProps, {createProject})(AddProject);