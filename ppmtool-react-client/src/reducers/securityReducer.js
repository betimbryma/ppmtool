import {SET_CURRENT_USER} from "../actions/types";

const initialState = {
    user: {},
    authenticated: false
};

const booleanActionPayload = (payload) => {
  if(payload)
      return true;
  return false;
};

export default function(state= initialState, action) {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                authenticated: booleanActionPayload(action.payload),
                user: action.payload
            };

        default:
            return state;
    }
}